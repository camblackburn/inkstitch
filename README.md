# InkStitch

[InkStitch](https://inkstitch.org/) is an open source digital embroidery software that's an extension to [Inkscape](https://inkscape.org/), an open source vector graphics tool. You can use InkStitch to convert vector graphics intro embroidery patterns that can be loaded into most digital embroidery machines.

## Installation
Follow the detailed instructions from InkStitch [here](https://inkstitch.org/docs/install/).

Make sure that you have Inkscape installed and that it is a version which is compatible with the correct version of InkStitch. For example, the latest release of InkStitch, v2.0.0, requires Inkscape v1.0.2 or higher - if you have an older version of Inkscape either upgrade or get an older version of InkStitch [here](https://inkstitch.org/categories/#releases).

## Quick Start
InkStitch is really well documented so instead of repeating everything they've already written here - just read the [workflow guide](https://inkstitch.org/docs/workflow/).

### Embroidering from an image
It's easiest to start with a vector image (.svg file) from the beginning, but most the time it's more convenient to start from a png.

You can convert a png to a vector image by using Inkscape Trace Bitmap tool. This will use a filtering method (you decide color, brightness, grays, or auto) to extract vector outlines from your image.

1. Import image (.png, .jpeg, .tiff, etc) into an Inkscape document: File > Import
2. Path > Trace Bitmap should open a new menu window
3. Select parameters for the best trace. If you want more than one path to be created, then select multiple scans. Usually, I find it easiest to scan by color, with n+1 number of scans for an image with n colors. You can update a live preview to see how it would turn out.
4. Finally, click "apply" and new paths will be generated for each trace, directly on top of the original image.

<img src="./media/trace_bitmap.jpg" height="400">

## Design tips

it's common to get an error message when trying to run inkstitch param command that says something like "path is not valid" - this usually happens if the borders cross over themselves somewhere. To get rid of this, first try breaking apart the object (Path > Break Apart) to split up and overlapping elements. If there were overlapping elements, they will appear as new objects in the list. Then delete any extra weird shapes (you may beed to toggle the fill / stroke parameters to see it properly). If that doesn't work, then try simplifying the path (Path > Simplify).

Use inset / outset to overlap the fill patterns properly or use overlap value in inkstitch params - but this bloats the whole pattern in usually an ugly way.

## Machine Guide
Instructions for Brother SE1800. This is both a sewing and embroidery machine, so before getting started make sure that the embroidery bed is loaded (the bigger on with the loop mount) and that the embroidery foot is on.

1. Export the embroidery file from Inkscape with File > Save a Copy > Select "Ink/Stitch: Tajima embroidery format (.dst)" file type and save the file to a usb.
2. Plus in usb to the machine and click the usb symbol on the screen (sometimes it takes a couple second for the software to realize a usb has been plugged in, just try again.)
3. Navigate to the file you want to embroider.
4. Make any edits to the size, orientation, or location of the pattern. The hoop size options are all shown in the upper left corner of the screen, The ones which are not grayed out are approved for that pattern, depending on its size and placement.

<img src="./media/edit_pattern.jpg" height="400">

5. once ready, click "end edit". It then let's you move the pattern while also positioning the stage (in case you need to do more precise alignment), when finished click "embroidery".
6. The next screen will open a list of all the thread changes that need to be made with a small drawing of what each layer
looks like and a time estimate.

<img src="./media/pattern_plans.jpg" height="400">

7. Thread the machine with desired color for the layer on the top of the list, lower the embroidery foot, and press the green arrow button to begin.

<img src="./media/machine.jpg" align="middle" height="500">

### Breaking Thread

When the thread breaks, press OK on the alert the opens on the screen. Then, move the stitch placement back 10 or so stitches so that you don't get a gap in the pattern because there's usually a delay between when the thread actually snapped and the machine recognizing it. Do so by clicking the button with the needle -/+ sign on the machine and selecting -10.

If it's the first time it's happens, just try rethreading it and start again, sometimes the thread just snaps and you can get away with not addressing the underlying issue.

If the thread continues to snap, it could be a whole host of reasons. Check the following:
- Try adjusting the tension in the machine - do this by clicking the spool +/- button. If the bobbin thread is being pulled up through the top of the fabric, decrease the tension. If the colored top thread is being pulled to the bottom, increase the tension. If you can't tell, just arbitrarily make changes until one works :)
- The needle could be dull, try putting a new needle in
- You could have too many layers embroidered on top of each other or a layer overlapping the edge of another, this would need to be modified with the design in inkstitch


## Examples
Venus fly trap patch - [.svg](./examples/venus.svg) inkscape file, [.dst](./examples/venus.dst) embroidery file

draw patch design in Illustrator on Ipad

<img src="./media/venus_drawing.jpg" height="400">

then convert into InkStitch file and proper embroidery settings

![venus simulation](./media/venus_simulation.mp4)

then embroider!

<img src="./media/venus_patch.jpg" height="400">

This diamond pattern shows how you can use the different fill angles to play with reflection / texture of a pattern to make different shapes while using the same color thread. To adjust the fill angle change the "angle of lines of stitches" in the param windows.

<img src="./media/diamond_sim.jpg" height="400">

<img src="./media/diamond_pattern.jpg" height="500">

### more examples
<img src="./media/haystack_embroidery.jpg" height="400">

<img src="./media/pineapple_skull.jpg" height="400">
